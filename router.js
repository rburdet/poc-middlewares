const router = require('express').Router();
const wrapAsync = require('express-async-handler')

router.get('/', wrapAsync( async () => {
  throw new Error('pepe');
}));
router.use((err,req,res,next) => res.send(500));

module.exports = router;
