const router = require('express').Router();
const wrapAsync = require('express-async-handler')

router.get('/mock', wrapAsync( async () => {
  throw new Error('pepe');
}));

module.exports = router;
